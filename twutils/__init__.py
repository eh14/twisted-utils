from twisted.python import log, threadable



import random
import time
from twisted.internet import defer, threads
import sys
from functools import wraps

from . import looper


def deferredSleep(reactor, duration):
    """
    Delays execution using deferreds. Mostly used in inline-callbacks.
    Idea taken from
    http://comments.gmane.org/gmane.comp.python.twisted/19394

    Use it like
    @defer.inlineCallbacks
    def foo():
        yield doSomething()
        # this will wait for 3 seconds without blocking anything
        yield testutils.deferredSleep(3)
        yield doSomethingAfterwards()

    """
    d = defer.Deferred()
    reactor.callLater(duration, d.callback, None)
    return d


def createLoopingCall(reactor, function, *args, **kwargs):
    """
    Creates a looping call
    """
    l = looper._LoggingLoopingCall(looper._SafeFunctionRunner(function, *args, **kwargs))
    l.clock = reactor

    return l



def startRetryingOneshotJob(reactor, function, args=(), kwargs={}, maxRetries=32, tryInterval=3.2):
    """
    Creates a oneshot job that executes once and tries maxRetries in case
    it crashes. After first successful execution it is being stopped.

    Best to use named parameters only!
    """

    l = looper._LoggingLoopingCall(looper._OneShotFunctionRunner(function, *args, **kwargs))
    l.clock = reactor
    l.start(interval=tryInterval,
                 now=False,
                 maxfails=maxRetries)

    return l

def runAsDeferredThread(func):
    """
    Decorates a function to run as a deferred thread when invoked in the reactor thread
    or reuse the same thread when invoked in a deferred thread already.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        if isReactorThread():
            # print "creating new thread"
            return threads.deferToThread(func, *args, **kwargs)
        else:
            # print "run it directly"
            result = func(*args, **kwargs)
            assert not isinstance(result, defer.Deferred), "The inner function must *not* be a deferred!"
            return result
    return wrapper


def isReactorThread():
    return threadable.isInIOThread()