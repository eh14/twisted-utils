"""
Global definitions used for setup
"""

__all__ = [
    "__title__", "__summary__", "__version__",
    "__author__", "__email__", "__uri__", "__license__",
    "__copyright__",
]


__title__ = "twisted-utils"
__summary__ = "Different utilities for python twisted."
__version__ = "0.1.1"

__author__ = "Franz Eichhorn"
__email__ = "frairon@googlemail.com"
__uri__='https://bitbucket.org/eh14/twisted-utils'
__license__ = "MIT"
__copyright__ = "2015 %s" % __author__



