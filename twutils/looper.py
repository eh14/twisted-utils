
import sys
import time
import traceback

from twisted.python import log
from twisted.internet import defer, task

class _SafeFunctionRunner(object):
    def __init__(self, function, *args, **kwargs):
        self.func = function
        self.args = args
        self.kwargs = kwargs

        self.job = None
        self.maxfails = None
        self.fails = 0

        # time in milliseconds the task
        # needs to be running before
        # the looper prints its time
        self._minLogTime = 50


    @defer.inlineCallbacks
    def __call__(self):
        try:
            start = time.time()
            result = yield self.func(*self.args, **self.kwargs)
            self.fails = 0
            defer.returnValue(result)
        except Exception as e:
            _exc_type, _exc_value, exc_traceback = sys.exc_info()
            tb = traceback.extract_tb(exc_traceback)
            exc_location = traceback.format_list(tb[-3:])
            log.msg('Exception occured running LoopingCall %s: %s\n(%s)' % (self.name, str(e), exc_location))
            self.fails += 1
        finally:
            looperTime = round((time.time() - start) * 1000, 2)
            if looperTime > self._minLogTime:
                log.msg('Looping call %s took %f ms' % (self.name, looperTime))

            if self.maxfails and self.fails >= self.maxfails:
                log.msg("Looper failed too many times (max %s). I'm giving up" % self.maxfails)
                self.job.stop()


    def setMinLogTime(self, minLogTime):
        self._minLogTime = minLogTime

    @property
    def name(self):
        return self.func.__name__

    def setMaxFails(self, job, maxfails):
        """
        Specify max number of fails along with the job that needs to be
        stopped when the max fails are reached.
        """
        assert job, "Need job to stop when max fails reached"
        assert maxfails > 0, "maxfails needs to be > 0"
        self.maxfails = maxfails
        self.job = job


class _OneShotFunctionRunner(_SafeFunctionRunner):

    @defer.inlineCallbacks
    def __call__(self):
        yield _SafeFunctionRunner.__call__(self)
        if self.fails == 0 and self.job.running:
            self.job.stop()


class _LoggingLoopingCall(task.LoopingCall):
    """
    Looping call that adds some jittering to the interval, thus avoiding to
    load the reactor more balanced.
    """

    def start(self, interval, now=True, maxfails=None, minLogTime=None):
        if maxfails:
            self.f.setMaxFails(self, maxfails)
        if minLogTime:
            self.f.setMinLogTime(minLogTime)


        return task.LoopingCall.start(self, interval, now)

    def stop(self, ignoreErrors=True):
        assert self.running or ignoreErrors, "Tried to stop non-running looper"

        if self.running:
            task.LoopingCall.stop(self)

    def isRunning(self):
        return self.running
