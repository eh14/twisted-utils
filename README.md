## Some utilities for Python Twisted ##


### Installation ###

The project is on pypi, so just do `pip install twisted-utils`

## Examples ##

Some example code is provided for the individual content items, more complex example are in the examples/ directory

## Contents ##

This library is merely a collection of utilities/convenience functions for python twisted that I found
useful enough for reuse.

### Safe Looping Call ###

Normally in twisted, when a looping call fails, the loop is terminated. This one logs the failure and goes on nonetheless

``` python

import twutils
from twisted.internet import reactor


func someFunc(arg1, arg2):
    pass

# create the looper
looper = twutils.createLooping(reactor, someFunc, "foo", arg2="bar")
# start it
looper.start(
    interval=1,
    now=True, # execute immediately or after first interval?
    maxfails=30, #stop after 30 failures. Failures are reset on success. Try forever if not passed
    minLogTime=500, # log the running time if longer than 500ms. Default is 50ms.
)

```

### Retrying Oneshot Timer ###

For tasks we need to get done once but which might fail a couple of times, there's the retrying oneshot timer.

``` python

import twutils


func someFunc(arg1, arg2):
    pass

# create the looper
looper = twutils.startRetryingOneshotJob(reactor, 
             someFunc, 
             args=("foo",), # positional arguments
             kwargs={"arg2":"bar"}, # keyword arguments
             maxRetries=10, # default is 32
             tryInterval=1, # try every second. Default is 3.2
         )
```

### Event Service ###

The event service provides inter-class/component communication using twisted's event loop.

Events are merely identified by simple strings.

``` python
from twutils import eventservice

# create an event service
events = eventservice.EventService()

# method handling the event.
# safest option is to use variable args, since the handler
# will be called with the arguments provided by the event triggerer.
def handler(*args, **kwargs):
    print "handler called"

# subscribe handler to the event
events.subscribeEvent("event", handler)

# trigger event
events.triggerEvent("event", 'arg1', arg2='bar')
```

Notes:

* event handlers are not executed right away but scheduled on the reactor with delay 0
* When handler objects are destroyed, the event subscription is deleted automatically (using weak refs), so you don't need to unsubscribe manually.
* all code runs in the reactor's thread, i.e. there are no race conditions.

### Time Provider ###

### Profiling Reactor ###

### Logging Site ###

### Convenience Functions ###

* runAsDeferredThread
* deferredSleep