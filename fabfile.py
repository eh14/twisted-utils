from fabric.decorators import task
from fabric.tasks import execute
from fabric.operations import local
from fabric.contrib.console import confirm

import os


here = os.path.abspath(os.path.dirname(__file__))

about = {}
# read about values
with open(os.path.join(here, 'twutils', '__about__.py'), 'r') as f:
    exec(f.read(), about)


@task
def check(test=''):
    testToRun = 'test%s' % ('.' + test if len(test) > 0 else '')

    local('trial --temp-directory=/tmp/_trial_tmp-twutils ' + testToRun)


@task
def localInstall():
    execute(check)
    local('sudo python setup.py install')

def incrementVersion(what):
    if what not in ['major', 'minor', 'micro']:
        raise Exception("Need at least one version number to increment")

    (major, minor, micro) = about['__version__'].split('.')
    if what == 'major':
        major = int(major) + 1
        minor = 0
        micro = 0
    elif what == 'minor':
        minor = int(minor) + 1
        micro = 0
    else:
        micro = int(micro) + 1

    newVersion = '%s.%s.%s' % (major, minor, micro)



    return newVersion

@task
def release(message, what='micro'):
    execute(check)
    newVersion = incrementVersion(what)

    if confirm("Current version is %s. New version would be %s. Uploading now?" % (about['__version__'], newVersion), default=True):
        local("sed -i 's/^__version__.*$/__version__ = \"%s\"/g' %s" % (newVersion, 'twutils/__about__.py'))

        local('python setup.py sdist')
        local('twine upload dist/twisted-utils-%s.tar.gz' % newVersion)

    if confirm('Commit it?', default=True):
        local('git commit -m "%s" .' % message)
        local('git tag -a v%s -m "%s"' % (newVersion, message))
