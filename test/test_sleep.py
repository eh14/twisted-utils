'''
Created on Nov 19, 2015

@author: eh14
'''

from twisted.trial import unittest
import twutils
from twisted.internet import defer, reactor
import time


class TestSleep(unittest.TestCase):

    @defer.inlineCallbacks
    def test_deferredSleep(self):
        """
        Tests the deferred sleep. For that we're using the real reactor...
        """
        start = time.time()
        for _ in range(3):
            yield twutils.deferredSleep(reactor, 0.1)

        # make sure we really slept
        self.assertTrue(time.time() - start > 0.3)

