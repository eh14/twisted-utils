'''
Created on Nov 19, 2015

@author: eh14
'''

from twisted.trial import unittest
import twutils
from twisted.internet.task import Clock


class TestLooper(unittest.TestCase):

    def setUp(self):
        self.runs = 0
        self.fails = 0

    def looperFailAfter(self, runs):
        """
        Test function that fails after passed runs
        """

        self.runs += 1
        if self.runs == runs:
            raise Exception("failed")


    def test_safeLooping_nofail(self):
        """
        schedules a looper that will never fail, check it runs as told.
        """
        cl = Clock()
        looper = twutils.createLoopingCall(cl, self.looperFailAfter, -1)

        looper.start(1, now=False, maxfails=1)

        for _ in range(10):
            cl.advance(1)

        self.assertEquals(10, self.runs)
        self.assertTrue(looper.isRunning())


    def test_safeLooping_fail(self):
        """
        Run a looper that fails after one trial. Check that is being
        stopped after the second.
        """
        cl = Clock()
        looper = twutils.createLoopingCall(cl, self.looperFailAfter, 1)

        looper.start(1, now=False, maxfails=1)

        cl.advance(1)
        cl.advance(1)

        self.assertEquals(1, self.runs)
        self.assertFalse(looper.isRunning())

    def oneShotAfter(self, fails):
        """
        Test function that succeeds after passed number of fails.
        """
        if self.fails < fails:
            self.fails += 1
            raise Exception("still failing")
        else:
            self.runs += 1
            return



    def test_oneshotFunction_success(self):
        cl = Clock()
        looper = twutils.startRetryingOneshotJob(cl,
                                                 self.oneShotAfter,
                                                 args=(1,),
                                                 maxRetries=2)
        for _ in range(10):
            cl.advance(1)

        self.assertEquals(1, self.fails)

        self.assertEquals(1, self.runs)
        self.assertFalse(looper.isRunning())



    def test_oneshotFunction_failing(self):
        cl = Clock()
        looper = twutils.startRetryingOneshotJob(cl,
                                                 self.oneShotAfter,
                                                 args=(1,),
                                                 maxRetries=1)


        for _ in range(10):
            cl.advance(1)

        self.assertEquals(0, self.runs)
        self.assertEquals(1, self.fails)
        self.assertFalse(looper.isRunning())
