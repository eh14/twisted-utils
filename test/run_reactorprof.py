'''

@author: eh14
'''
from twutils import reactorprof
import time
import subprocess
from twisted.internet import defer, threads

reactorprof.install() 


def long():
    print "sleeping"
    time.sleep(0.1)
    
    
def exit():
    print "stopping"
    reactor.stop()
    
def externalWait():
    p = subprocess.Popen(['sleep 0.1'], shell=True)
    p.wait()
    
@defer.inlineCallbacks
def waitExternal():
    for i in range(10):
        yield threads.deferToThread(externalWait)
        
    


if __name__ == '__main__':
    from twisted.internet import reactor  
    
    reactor.callLater(0, long)
    reactor.callLater(0, waitExternal)
    reactor.callLater(3, exit)
    reactor.run()  # @UndefinedVariable