'''
Created on Nov 19, 2015

@author: eh14
'''

from twisted.trial import unittest
import twutils
from twisted.internet import defer
from twisted.python import threadable

class TestIOThread(unittest.TestCase):

    @defer.inlineCallbacks
    def test_iothread(self):
        # the unittest is not the eractor thread
        self.assertFalse(twutils.isReactorThread())
        # so
        res = yield self._deferred()
        self.assertFalse(res, "the deferred should not go to a thread, since the main thread thinks its not the reactor")


        # ... unless we register ist, then it behaves as the real application
        threadable.registerAsIOThread()
        self.assertTrue(twutils.isReactorThread())
        res = yield self._deferred()
        self.assertFalse(res, "The deferred-thread shouldn't think it's the reactor.")


    @twutils.runAsDeferredThread
    def _deferred(self):
        return twutils.isReactorThread()

